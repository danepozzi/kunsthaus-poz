#!/bin/sh

me=`basename "$0"`
echo "Start script $me"
echo "slot is $1, group is $2, last IP part is $3, rpi index is $4"
export PI_IDX=$4

sleep 2

echo "kill everything"
killall sclang
killall scsynth
killall qjackctl
killall jackd

sleep 2

echo "scheduling reboots"
at 12:00 -f /home/pi/Documents/projects/ThruSegm/reboot.sh
at 14:00 -f /home/pi/Documents/projects/ThruSegm/reboot.sh
at 16:00 -f /home/pi/Documents/projects/ThruSegm/reboot.sh
at 18:00 -f /home/pi/Documents/projects/ThruSegm/shutdown.sh

sleep 2

echo "setting ALSA"
amixer -q -c1 set 'PCM Capture Source' Line
amixer -q -c1 set Line cap
amixer -q -c1 set Speaker Playback 101
amixer -q -c1 set Line Capture 4096

sleep 2

echo "start SC"
sclang -u 57120 /home/pi/Documents/projects/ThruSegm/poz/200830/200830_setup.scd
