#!/bin/sh

echo "start POZ"

sleep 2

echo "kill everything"
killall sclang
killall scsynth
killall qjackctl
killall jackd

sleep 2

echo "setting ALSA"
amixer -q -c1 set 'PCM Capture Source' Line
amixer -q -c1 set Line cap
amixer -q -c1 set Speaker Playback 101
amixer -q -c1 set Line Capture 4096

sleep 2

echo "start SC"
sclang -u 57120 /home/pi/Documents/projects/ThruSegm/poz/200830/200830_setup.scd
