//osc responder to revive servers if dead
OSCdef(\reboot, {|msg, time, addr, recvPort|
	if(~dead == true) {
		~dead = false;
		fork({
			\REBOOTING.postln;
			5.wait;
			("/home/pi/Documents/segdir/cube/200710/200830_setup.scd").load;
		}, AppClock);
	}
}, '/servergone');

OSCFunc({|msg, time, addr| msg.postln; addr.postln}, '/active');

//send
m = NetAddr("192.168.0.77", 57120); // hhr
//m = NetAddr("192.168.0.32", NetAddr.langPort); // hhr
n = NetAddr("192.168.0.29", 57120); // ji

OSCFunc({|msg, time, addr|
	msg.postln;
	m.sendMsg("/active", "poz", 0);
	n.sendMsg("/active", "poz", 0);
}, '/silence');

OSCFunc({|msg, time, addr|
	msg.postln;
	m.sendMsg("/active", "poz", 1);
	n.sendMsg("/active", "poz", 1);
}, '/sound');

//m.sendMsg("/active", "poz", 1);
//n.sendMsg("/active", "poz", 1);